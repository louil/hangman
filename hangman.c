#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#define MAX 36

void get_word(char word[MAX], int argc, char *argv[]);
void is_hangman(void);
void print_func(int totallen, char underline[MAX]);
void compare(int *totallen, int *numguess, int *wrongguess, char word[MAX], char userword[MAX], char underline[MAX]);
void print_gallows(int *wrongguess);
void edit_hangman(int *win, int *lose, int *wrongguess);

int main(int argc, char *argv[]){

	//Setting variable to be used throughout the program
	int totallen = 0, guesswordlen = 0, numguess = 0, wrongguess = 0; 
	char word[MAX] = {0};
	char userword[MAX] = {0};
	char underline[MAX] = {0};

	if (argc > 2){
		exit(0);
	}

	
	//Calling functions to perform the program
	get_word(word, argc, argv);
	is_hangman();
	totallen = strlen(word);
	print_gallows(&wrongguess);	
	print_func(totallen, underline);

	while(1){

		beginning:
	
			//Grab a word from the user
			userword[0] = getchar();

			if (userword[0] == '\n'){
				print_gallows(&wrongguess);
				printf("%d %s: ", numguess, underline);
				goto beginning;
			}	

			while(getchar() != '\n');

			//Checking to see if user input was a number and if so go back to beggining 
			if (userword[0] >= '0' && userword[0] <= '9'){
				print_gallows(&wrongguess);
				printf("%d %s: ", numguess, underline);
				goto beginning;
			}
			//Checking to see if user input was a capital word and if so go back to beggining
			else if (userword[0] >= 'A' && userword[0] <= 'Z'){
				userword[0] = userword[0] + 32;
			}
			else if (userword[0] == '\n'){
				print_gallows(&wrongguess);
				printf("%d %s: ", numguess, underline);
				goto beginning;
			}			

			guesswordlen = strlen(userword);
			printf("%s\n", userword);
			printf("%d\n", guesswordlen);

			//If the guessword length is not 1 then go to beggining
			if (guesswordlen != 1){
				print_gallows(&wrongguess);
				printf("%d %s: ", numguess, underline);
				goto beginning;
			}
			compare(&totallen, &numguess, &wrongguess, word, userword, underline);
			print_gallows(&wrongguess);
			printf("%d %s: ", numguess, underline);
	}
	
}

void get_word(char word[MAX], int argc, char *argv[]){

	int count = 0, loopcounter = 0, rando = 0;
	char line[36];
	char pointtohome[100];

	srand(time(NULL));

	FILE *fp;

	//Create the method to get to the .wordlist file and if not there create one

	if (argc == 1) {
		strcpy(pointtohome, getenv("HOME"));
		strcat(pointtohome, "/.words");
	}
	else if (argc == 2){
		strcpy(pointtohome, argv[1]);
	}
	
	fp = fopen(pointtohome, "r"); 
	if (fp == NULL){
		perror("Would not open.\n");
		exit(0);
    }

	//The following while loop found at
	//http://stackoverflow.com/questions/232237/whats-the-best-way-to-return-a-random-line-in-a-text-file-using-c
	while (fgets(line, 36, fp) != NULL){
    	count++;
	}
	
	//It sets rando to a random number % by count + 1. Then seek back to the top of the file
	rando = ((rand() % count) + 1);
	fseek(fp, 0, SEEK_SET);	

	//Loops back through the file and if rando is equal to the loopcounter copy the word on the line into the char array word
	while (fgets(line, 36, fp) != NULL){
		loopcounter++;
    	if (rando == loopcounter){
        	strncpy(word, line, 36);
		}
	}
	fclose(fp);
}

void is_hangman(){
	
	//The basis of the following block of code was found at http://www.programmingsimplified.com/c-program-read-file 
	char pointtohome[100];
	int number[5] = {0};
	int index = 0;
	double average = 0.0;

	FILE *fp;

	//Create the method to get to the .hangman file and if not there create one
	strcpy(pointtohome, getenv("HOME"));
	strcat(pointtohome, "/.hangman");

	fp = fopen(pointtohome, "r"); 
	if (fp == NULL){
    	fp = fopen(pointtohome, "w");
		fprintf(fp, "1000");
    }

	//Loop through the file turning what is stored at number[index] as a number as long as it does not hit EOF 
	while((number[index] = fgetc(fp)) != EOF){
		number[index] -= '0';
		index++;
	}
	
	average = number[3] * 1.0;

	//The following code in this function is mine
	//Print out the information for the player to know
	if (number[0] == -1){
		number[0] = 1;
	}
	printf("Game %d. ", number[0]);
	if(number[1] > 1){
		printf("%d Wins/%d Losses. Average score: %.1f\n", number[1], number[2], average);
	}
	else{
		printf("%d Win/%d Losses. Average score: %.1f\n", number[1], number[2], average);
	}
	fclose(fp);
}

void print_func(int totallen, char *underline){
	
	int i = 0, startguess = 0;

	//Loop through and print out _ for how long the word to be guessed is
	for (i = 0; i < (totallen - 1); i++){
		underline[i] = '_';
	}

	printf("%d %s: ", startguess, underline);
}

void compare(int *totallen, int *numguess, int *wrongguess, char word[MAX], char userword[MAX], char underline[MAX]){

	int z = 0, t = 0, rightword = 0, isword = 0, win = 0, lose = 0;
	
	*numguess += 1;

	//Loop through and if the word equals the userword then replace the underline at the spot that the word is
	for (z = 0; z < *totallen; z++ ){
		if (word[z] == *userword){
			underline[z] = *userword;
			isword += 1;	
		}
	}

	if (isword == 0){
		*wrongguess += 1;
	}
	
	//Setting rightword to 1 if underline was replaced in the previous for loop, otherwise rightword is set to 0 and break
	for (t = 0; t < (*totallen - 1); t++ ){
		if (underline[t] == word[t]){
			rightword = 1;	
		}
		else{
			rightword = 0;
			break;
		}
	}

	//If rightword was set to 1 then print the game was won with only 1 wrong guess print "miss" otherwise print "misses"
	if (rightword == 1){
		if (*wrongguess == 1){
			printf("      \\O/\n");
			printf("       |\n");
			printf("      / \\\n");
			printf("You win! You had %d miss.\n", *wrongguess);
			win = 1;
			edit_hangman(&win, &lose, wrongguess);
			exit(0);
		}
		else{
			printf("      \\O/\n");
			printf("       |\n");
			printf("      / \\\n");
			printf("You win! You had %d misses.\n", *wrongguess);
			win = 1;
			edit_hangman(&win, &lose, wrongguess);
			exit(0);
		}
	}
	
}

void print_gallows(int *wrongguess){

	int win = 0, lose = 0;	

	//Created switch case to track the number of wrongguess and print out the gallows with the picture of the stick man being hanged
	//and if wrongguess was 6 then the stick man would be hanged and the game would be lost and the program would exit
	switch(*wrongguess){
		case 0:
			printf("          __________\n");
			printf("          |    |\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("       ___|___\n\n");
			break;
		case(1):
			printf("          __________\n");
			printf("          |    |\n");
			printf("          |    O\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("       ___|___\n\n");
			break;
		case 2:
			printf("          __________\n");
			printf("          |    |\n");
			printf("          |    O\n");
			printf("          |    |\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("       ___|___\n\n");
			break;
		case 3:
			printf("          __________\n");
			printf("          |    |\n");
			printf("          |    O\n");
			printf("          |   /|\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("       ___|___\n\n");
			break;
		case(4):
			printf("          __________\n");
			printf("          |    |\n");
			printf("          |    O\n");
			printf("          |   /|\\\n");
			printf("          |\n");
			printf("          |\n");
			printf("          |\n");
			printf("       ___|___\n\n");
			break;
		case 5:
			printf("          __________\n");
			printf("          |    |\n");
			printf("          |    O\n");
			printf("          |   /|\\\n");
			printf("          |   /\n");
			printf("          |\n");
			printf("          |\n");
			printf("       ___|___\n\n");
			break;
		case 6:
			printf("          __________\n");
			printf("          |    |\n");
			printf("          |    O\n");
			printf("          |   /|\\\n");
			printf("          |   / \\\n");
			printf("          |\n");
			printf("          |\n");
			printf("       ___|___\n");
			printf("You lose!\n");
			lose = 1;
			edit_hangman(&win, &lose, wrongguess);
			exit(0);
	}
}

void edit_hangman(int *win, int *lose, int *wrongguess){

	char pointtohome[100];
	int number[5] = {0};
	int index = 0;
	int average = 0;

	FILE *fp;

	//Create the method to get to the .hangman file and if not there create one
	strcpy(pointtohome, getenv("HOME"));
	strcat(pointtohome, "/.hangman");

	fp = fopen(pointtohome, "r");  
	
	while((number[index] = fgetc(fp)) != EOF){
		number[index] -= '0';
		index++;
	}
	
	fclose(fp);

	fp = fopen(pointtohome, "w"); 
	
	if (*wrongguess == 6){
		goto end;
	}
	else if (number[1] == 0 || *wrongguess == 0){
		average = *wrongguess;
		goto end;
	}

	average = (((number[1])*(number[3])+*wrongguess) / (number[1] + 1));

	end:
		//Edit the .hangman file and add where needed
		number[0] += 1;
		if (*win == 1){
			number[1] += 1;
			fprintf(fp, "%d%d%d%d", number[0], number[1], number[2], average);
		}
		else if (*lose == 1){
			average = (int)number[3];
			number[2] += 1;
			fprintf(fp, "%d%d%d%d", number[0], number[1], number[2], average);
		}
	
		fclose(fp);
}


