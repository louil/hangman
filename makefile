CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic

.PHONY: clean debug

hangman: hangman.o 

debug: CFLAGS+=-g
debug: hangman


clean:
	-rm hangman *.o
